package net.franksreich

import akka.actor.{ ActorRef, Props }
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import net.franksreich.database.Database
import net.franksreich.database.access.{ EquipmentDataAccess, ExerciseDataAccess, UserDataAccess }
import net.franksreich.database.model.Users
import net.franksreich.user.{ UserActor, UserRoutes }
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, WordSpec }
import akka.util.Timeout
import org.bson.types.ObjectId

import scala.concurrent.duration._
import scala.concurrent.Future

class UserRoutesSpec
  extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest {

  class TestUserDataAccess extends UserDataAccess {
    def users(skip: Int, limit: Int) = Future(Users(List()))
  }

  class TestExerciseDataAccess extends ExerciseDataAccess {
    def exercises(skip: Int, limit: Int) = Future((List(), false))
  }

  class TestEquipmentDataAccess extends EquipmentDataAccess {
    def equipment(id: ObjectId) = Future(None)
    def equipments(skip: Int, limit: Int) = Future((List(), true))
  }

  class TestDatabase extends Database {
    val userDataAccess = new TestUserDataAccess()
    val exerciseDataAccess = new TestExerciseDataAccess()
    val equipmentDataAccess = new TestEquipmentDataAccess()
  }

  val userActor: ActorRef =
    system.actorOf(Props(new UserActor(new TestDatabase())), "userRegistry")

  val userRoutes = new UserRoutes(system, userActor, Timeout(5.seconds))

  lazy val routes = userRoutes.routes

  "UserRoutes" should {
    "return no users if no present (GET /user)" in {
      val request = HttpRequest(uri = "/user")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)

        contentType should ===(ContentTypes.`application/json`)

        entityAs[String] should ===("""{"users":[]}""")
      }
    }
  }
}
