package net.franksreich.database.access

import scala.concurrent.Future
import akka.event.LoggingAdapter
import net.franksreich.database.access.shared.ReadMultiple
import org.mongodb.scala.{ Document, MongoDatabase }
import net.franksreich.database.model.{ Exercise, ExerciseEquipment }

trait ExerciseDataAccess {
  def exercises(skip: Int, limit: Int): Future[(Seq[Exercise], Boolean)]
}

class ExerciseDataAccessImplementation(
  private val database: MongoDatabase,
  protected val log: LoggingAdapter)
  extends ExerciseDataAccess with ReadMultiple[Exercise] {

  protected val collection = database.getCollection("exercise")

  def exercises(skip: Int, limit: Int) = readAndHasMore(skip, limit)

  private def documentToExercise(document: Document) = {
    val id = document("_id").asObjectId().getValue
    val name = document("name").asString().getValue
    val equipmentDocument = new Document(document("equipment").asDocument())
    val equipmentId = equipmentDocument("id").asObjectId().getValue
    val equipmentName = equipmentDocument("name").asString().getValue
    Exercise(id, name, ExerciseEquipment(equipmentId, equipmentName))
  }

  protected val documentToT = documentToExercise
}
