package net.franksreich.database

import akka.event.LoggingAdapter
import org.mongodb.scala.model.CountOptions
import scala.concurrent.ExecutionContext
import org.mongodb.scala.{ Document, MongoCollection }
import scala.concurrent.Future

package object access {
  /*
  def readAndHasNext[T](
    collection: MongoCollection[Document],
    log: LoggingAdapter,
    documentToT: Document => T,
    skip: Int,
    limit: Int): Future[(Seq[T], Boolean)] = {

    import ExecutionContext.Implicits.global
    val countOptions = new CountOptions().skip(skip + limit)
    val emptyFilter = Document()
    val hasMoreFuture = collection.count(emptyFilter, countOptions)
      .toFuture().map(count => count > 0)
    val tFuture = collection.find().skip(skip).limit(limit).collect().toFuture()
      .map(documents => {
        log.info(s"Retrieved ${documents.size}")
        documents.map(document => documentToT(document))
      })
    tFuture.zip(hasMoreFuture)
  }
  */
}
