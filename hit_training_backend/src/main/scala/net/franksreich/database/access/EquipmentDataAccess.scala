package net.franksreich.database.access

import akka.event.LoggingAdapter
import net.franksreich.database.access.shared.ReadMultiple
import net.franksreich.database.model.{ Equipment, Pound, WeightStack }
import org.bson.types.ObjectId
import org.mongodb.scala.model.Filters
import org.mongodb.scala.{ Document, MongoDatabase }
import scala.concurrent.ExecutionContext
import scala.collection.JavaConverters._
import scala.concurrent.Future

trait EquipmentDataAccess {
  def equipments(skip: Int, limit: Int): Future[(Seq[Equipment], Boolean)]
  def equipment(id: ObjectId): Future[Option[Equipment]]
}

class EquipmentDataAccessImplementation(
  private val database: MongoDatabase,
  protected val log: LoggingAdapter)
  extends EquipmentDataAccess with ReadMultiple[Equipment] {

  protected val collection = database.getCollection("equipment")

  def equipments(skip: Int, limit: Int) = readAndHasMore(skip, limit)

  def equipment(id: ObjectId) = {
    import ExecutionContext.Implicits.global
    collection.find(Filters.eq("_id", id)).toFuture().map(documents =>
      if (documents.nonEmpty) {
        Some(documentToT(documents.head))
      } else {
        None
      })
  }

  private def documentToEquipment(document: Document) = {
    val id = document("_id").asObjectId().getValue
    val name = document("name").asString().getValue
    val weightStackDocument = new Document(document("weightStack").asDocument())
    val weightStack = documentToWeightStack(weightStackDocument)
    Equipment(id, name, weightStack)
  }

  private def documentToWeightStack(document: Document) = {
    val weightStackUnit = document("unit").asString().getValue match {
      case "lbs" => Pound
    }
    val weightStackMinimum = document("minimum").asDouble().getValue
    val weightStackIncrements = document("increment").asArray()
      .asScala.map(increment => increment.asDouble().getValue)
    if (document.contains("maximum")) {
      val weightStackMaximum = document("maximum").asDouble().getValue
      WeightStack(
        weightStackMinimum, Some(weightStackMaximum), weightStackIncrements, weightStackUnit)
    } else {
      WeightStack(weightStackMinimum, None, weightStackIncrements, weightStackUnit)
    }
  }

  protected val documentToT = documentToEquipment
}
