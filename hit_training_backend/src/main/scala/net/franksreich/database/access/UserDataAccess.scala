package net.franksreich.database.access

import scala.concurrent.{ Future, ExecutionContext }
import scala.collection.JavaConverters._

import akka.event.LoggingAdapter

import org.mongodb.scala.MongoDatabase

import net.franksreich.database.model._

trait UserDataAccess {
  def users(skip: Int, limit: Int): Future[Users]
}

class UserDataAccessImplementation(
  private val database: MongoDatabase,
  private val log: LoggingAdapter) extends UserDataAccess {

  private val collection = database.getCollection("user")

  def users(skip: Int, limit: Int) = {
    log.info(s"Retrieving users with skip = $skip, limit = $limit")
    import ExecutionContext.Implicits.global
    collection.find().skip(skip).limit(limit).collect().toFuture().map(documents => {
      log.info(s"Retrieved ${documents.size} user documents")
      val users = documents.map(document => {
        val id = document("_id").asObjectId().getValue
        val name = document("name").asString().getValue
        val eMail = document("e-mail").asString().getValue
        val sex = document("sex").asString().getValue match {
          case "Male" => Male
          case "Female" => Female
        }
        val roles = document("roles").asArray().getValues.asScala.map(value => {
          value.asString().getValue match {
            case "Athlete" => Athlete
            case "Trainer" => Trainer
          }
        })
        User(id, name, sex, eMail, roles)
      })
      Users(users)
    })
  }
}
