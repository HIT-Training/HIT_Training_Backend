package net.franksreich.database.access.shared

import akka.event.LoggingAdapter
import org.mongodb.scala.model.CountOptions
import scala.concurrent.ExecutionContext
import org.mongodb.scala.{ Document, MongoCollection }

trait ReadMultiple[T] {
  protected val log: LoggingAdapter
  protected val collection: MongoCollection[Document]
  protected val documentToT: Document => T

  def readAndHasMore(skip: Int, limit: Int) = {
    readMultiple(skip, limit).zip(hasMore(skip, limit))
  }

  private def readMultiple(skip: Int, limit: Int) = {
    import ExecutionContext.Implicits.global
    collection.find().skip(skip).limit(limit).collect().toFuture()
      .map(documents => {
        log.info(s"Retrieved ${documents.size} documents")
        documents.map(document => documentToT(document))
      })
  }

  private def hasMore(skip: Int, limit: Int) = {
    import ExecutionContext.Implicits.global
    val countOptions = new CountOptions().skip(skip + limit)
    val emptyFilter = Document()
    collection.count(emptyFilter, countOptions).toFuture().map(count => count > 0)
  }
}
