package net.franksreich.database.model

import org.bson.types.ObjectId

sealed trait Sex { def name: String }
case object Male extends Sex { val name = "Male" }
case object Female extends Sex { val name = "Female" }

sealed trait Role { def name: String }
case object Athlete extends Role { val name = "Athlete" }
case object Trainer extends Role { val name = "Trainer" }

final case class User(
  _id: ObjectId, name: String, sex: Sex, eMail: String, roles: Seq[Role])

final case class Users(users: Seq[User])
