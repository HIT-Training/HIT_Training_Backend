package net.franksreich.database.model

import org.bson.types.ObjectId

final case class ExerciseEquipment(id: ObjectId, name: String)
final case class Exercise(_id: ObjectId, name: String, equipment: ExerciseEquipment)
