package net.franksreich.database.model

import org.bson.types.ObjectId

sealed trait WeightUnit {
  def name: String
  def abbreviation: String
}

case object Pound extends WeightUnit {
  val name = "Pound"
  val abbreviation = "lbs"
}

case class WeightStack(
  minimum: Double, maximum: Option[Double], increment: Seq[Double], unit: WeightUnit)
case class Equipment(_id: ObjectId, name: String, weightStack: WeightStack)
