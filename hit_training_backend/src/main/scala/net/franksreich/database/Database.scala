package net.franksreich.database

import akka.event.LoggingAdapter
import net.franksreich.database.access._
import net.franksreich.database.codecs.CustomCodecRegistries
import org.mongodb.scala.MongoClient

trait Database {
  def userDataAccess: UserDataAccess
  def exerciseDataAccess: ExerciseDataAccess
  def equipmentDataAccess: EquipmentDataAccess
}

class DatabaseImplementation(
  private val url: String,
  private val databaseName: String,
  private val log: LoggingAdapter) extends Database {

  private val client = MongoClient(url)
  private val database = client
    .getDatabase(databaseName)
    .withCodecRegistry(CustomCodecRegistries.codecRegistry)

  val userDataAccess = new UserDataAccessImplementation(database, log)
  val exerciseDataAccess = new ExerciseDataAccessImplementation(database, log)
  val equipmentDataAccess = new EquipmentDataAccessImplementation(database, log)
}
