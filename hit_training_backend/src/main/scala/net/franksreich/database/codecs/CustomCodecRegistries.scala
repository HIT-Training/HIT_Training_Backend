package net.franksreich.database.codecs

import org.bson.UuidRepresentation
import org.bson.codecs.UuidCodec
import org.bson.codecs.configuration.CodecRegistries
import org.mongodb.scala.MongoClient

object CustomCodecRegistries {
  val uuidCodecRegistry = CodecRegistries.fromCodecs(
    new UuidCodec(UuidRepresentation.STANDARD))

  val codecRegistry = CodecRegistries.fromRegistries(
    uuidCodecRegistry,
    MongoClient.DEFAULT_CODEC_REGISTRY)
}
