package net.franksreich.shared

import akka.http.scaladsl.model.Uri
import org.bson.types.ObjectId
import spray.json.{ JsString, JsValue, RootJsonFormat, deserializationError }

trait JsonSupport {
  import spray.json.DefaultJsonProtocol._

  implicit object UriJsonFormat extends RootJsonFormat[Uri] {
    def write(uri: Uri) = JsString(uri.toString())
    def read(value: JsValue) = value match {
      case JsString(x) => Uri(x)
      case x => deserializationError(s"$x is not a valid uri")
    }
  }

  implicit val OptionUriFormat = optionFormat(UriJsonFormat)

  implicit object ObjectIdJsonFormat extends RootJsonFormat[ObjectId] {
    def write(objectId: ObjectId): JsValue = JsString(objectId.toHexString)
    def read(value: JsValue) = value match {
      case JsString(x) => new ObjectId(x)
      case x => deserializationError(s"$x is not a valid Object Id")
    }
  }
}
