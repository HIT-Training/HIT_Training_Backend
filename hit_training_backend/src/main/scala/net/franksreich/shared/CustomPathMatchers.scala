package net.franksreich.shared

import akka.http.scaladsl.server.PathMatcher
import org.bson.types.ObjectId

object CustomPathMatchers {
  val objectIdMatcher = {
    PathMatcher("[0-9,a-z]{24}".r).flatMap { path => Some(new ObjectId(path)) }
  }
}
