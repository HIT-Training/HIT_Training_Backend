package net.franksreich.shared

import akka.http.scaladsl.model.HttpHeader

trait HeaderExtractor {
  def extractServiceBase(header: HttpHeader) = header match {
    case HttpHeader("service-base", value) => Some(value)
    case _ => None
  }
}
