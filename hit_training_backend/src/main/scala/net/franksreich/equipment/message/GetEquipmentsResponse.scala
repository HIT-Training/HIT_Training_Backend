package net.franksreich.equipment.message

import akka.http.scaladsl.model.Uri
import net.franksreich.database.model.Equipment

case class GetEquipmentsResponse(equipments: Seq[Equipment], next: Option[Uri])
