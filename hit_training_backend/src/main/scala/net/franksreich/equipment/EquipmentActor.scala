package net.franksreich.equipment

import akka.actor.{ Actor, ActorLogging }
import net.franksreich.database.Database
import org.bson.types.ObjectId

object EquipmentActor {
  final case class GetEquipments(skip: Int, limit: Int)
  final case class GetEquipment(id: ObjectId)
}

class EquipmentActor(val database: Database) extends Actor with ActorLogging {
  import EquipmentActor._

  def receive = {
    case GetEquipment(id) =>
      sender() ! database.equipmentDataAccess.equipment(id)
    case GetEquipments(skip, limit) =>
      sender() ! database.equipmentDataAccess.equipments(skip, limit)
  }
}
