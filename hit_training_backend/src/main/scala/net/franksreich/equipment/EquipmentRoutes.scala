package net.franksreich.equipment

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.server.Directives
import akka.util.Timeout
import net.franksreich.database.model.Equipment
import net.franksreich.equipment.EquipmentActor.{ GetEquipment, GetEquipments }
import net.franksreich.shared.HeaderExtractor
import akka.pattern.ask
import net.franksreich.equipment.message.GetEquipmentsResponse
import org.bson.types.ObjectId

import scala.concurrent.{ ExecutionContext, Future }
import net.franksreich.shared.CustomPathMatchers

class EquipmentRoutes(
  private val system: ActorSystem,
  private val equipmentActor: ActorRef,
  private implicit val timeout: Timeout,
  private val baseUrl: String)
  extends Directives with EquipmentJsonSupport with HeaderExtractor {

  import CustomPathMatchers._

  lazy val log = Logging(system, classOf[EquipmentRoutes])

  lazy val routes =
    path("equipment" / objectIdMatcher) { id =>
      get {
        val result = (equipmentActor ? GetEquipment(id)).mapTo[Future[Option[Equipment]]]
        complete(result)
      }
    } ~
      path("equipment") {
        (headerValue(extractServiceBase) | provide(baseUrl)) { serviceBase =>
          log.info(s"Using service root: $serviceBase")
          get {
            parameters('skip.as[Int] ? 0, 'limit.as[Int] ? 20) { (skip, limit) =>
              val result = (equipmentActor ? GetEquipments(skip, limit))
                .mapTo[Future[(Seq[Equipment], Boolean)]].flatten
              import ExecutionContext.Implicits.global
              val response = result.map(pair =>
                if (pair._2) {
                  val newSkip = skip + limit
                  GetEquipmentsResponse(
                    pair._1, Some(Uri(s"$serviceBase/equipment?skip=$newSkip&limit=$limit")))
                } else {
                  GetEquipmentsResponse(pair._1, None)
                })
              complete(response)
            }
          }
        }
      }
}
