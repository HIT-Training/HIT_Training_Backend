package net.franksreich.equipment

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._
import net.franksreich.database.model.{ Equipment, Pound, WeightStack, WeightUnit }
import net.franksreich.equipment.message.GetEquipmentsResponse
import net.franksreich.shared.JsonSupport

trait EquipmentJsonSupport extends SprayJsonSupport with JsonSupport {
  import DefaultJsonProtocol._

  implicit object WeightUnitFormat extends JsonFormat[WeightUnit] {
    def write(weightUnit: WeightUnit) = JsString(weightUnit.abbreviation)
    def read(value: JsValue) = value match {
      case JsString("lbs") => Pound
      case x => deserializationError(s"$x is not a weight unit")
    }
  }

  implicit val weightStackFormat = jsonFormat4(WeightStack)
  implicit val equipmentFormat = jsonFormat3(Equipment)
  implicit val getEquipmentsResponseFormat = jsonFormat2(GetEquipmentsResponse)
}
