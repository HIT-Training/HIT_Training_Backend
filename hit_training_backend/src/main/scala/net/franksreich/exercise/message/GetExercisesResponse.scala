package net.franksreich.exercise.message

import akka.http.scaladsl.model.Uri
import net.franksreich.database.model.Exercise

case class GetExercisesResponse(exercises: Seq[Exercise], next: Option[Uri])
