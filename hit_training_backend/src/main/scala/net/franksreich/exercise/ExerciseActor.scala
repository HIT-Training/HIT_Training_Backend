package net.franksreich.exercise

import akka.actor.{ Actor, ActorLogging }
import net.franksreich.database.Database

object ExerciseActor {
  final case class GetExercises(skip: Int, limit: Int)
}

class ExerciseActor(val database: Database) extends Actor with ActorLogging {
  import ExerciseActor._

  def receive = {
    case GetExercises(skip, limit) =>
      sender() ! database.exerciseDataAccess.exercises(skip, limit)
  }
}
