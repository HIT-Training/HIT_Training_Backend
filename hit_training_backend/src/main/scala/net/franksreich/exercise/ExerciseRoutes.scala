package net.franksreich.exercise

import akka.actor.{ ActorRef, ActorSystem }
import akka.pattern.ask
import akka.event.Logging
import akka.http.scaladsl.model.{ HttpHeader, HttpResponse, Uri }
import akka.http.scaladsl.server.Directives
import akka.util.Timeout
import net.franksreich.database.model.Exercise
import net.franksreich.exercise.ExerciseActor.GetExercises
import net.franksreich.exercise.message.GetExercisesResponse
import net.franksreich.shared.HeaderExtractor

import scala.concurrent.{ ExecutionContext, Future }

class ExerciseRoutes(
  private val system: ActorSystem,
  private val exerciseActor: ActorRef,
  private implicit val timeout: Timeout,
  private val baseUrl: String)
  extends Directives with ExerciseJsonSupport with HeaderExtractor {

  lazy val log = Logging(system, classOf[ExerciseRoutes])

  lazy val routes =
    path("exercise") {
      (headerValue(extractServiceBase) | provide(baseUrl)) { serviceBase =>
        log.info(s"Using service root: $serviceBase")
        get {
          parameters('skip.as[Int] ? 0, 'limit.as[Int] ? 20) { (skip, limit) =>
            val result = (exerciseActor ? GetExercises(skip, limit))
              .mapTo[Future[(Seq[Exercise], Boolean)]].flatten
            import ExecutionContext.Implicits.global
            val response = result.map(pair =>
              if (pair._2) {
                val newSkip = skip + limit
                GetExercisesResponse(
                  pair._1, Some(Uri(s"$serviceBase/exercise?skip=$newSkip&limit=$limit")))
              } else {
                GetExercisesResponse(pair._1, None)
              })
            complete(response)
          }
        }
      }
    }
}
