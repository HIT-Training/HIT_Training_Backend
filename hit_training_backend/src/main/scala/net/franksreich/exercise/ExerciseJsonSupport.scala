package net.franksreich.exercise

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import net.franksreich.database.model.{ Exercise, ExerciseEquipment }
import net.franksreich.exercise.message.GetExercisesResponse
import net.franksreich.shared.JsonSupport

trait ExerciseJsonSupport extends SprayJsonSupport with JsonSupport {
  import spray.json.DefaultJsonProtocol._

  implicit val exerciseEquipmentJsonFormat = jsonFormat2(ExerciseEquipment)
  implicit val exerciseJsonFormat = jsonFormat3(Exercise)
  implicit val getExercisesResponseFormat = jsonFormat2(GetExercisesResponse)
}
