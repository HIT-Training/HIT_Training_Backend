package net.franksreich.user

import scala.concurrent.Future
import akka.pattern.ask
import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.server.ValidationRejection
import akka.util.Timeout
import net.franksreich.database.model.{ User, Users }
import net.franksreich.user.UserActor.{ GetUser, GetUsers }

class UserRoutes(
  private val system: ActorSystem,
  private val userActor: ActorRef,
  private implicit val timeout: Timeout)
  extends Directives with UserJsonSupport {

  lazy val log = Logging(system, classOf[UserRoutes])

  lazy val routes =
    path("user" / JavaUUID) { guid =>
      get {
        val maybeUser: Future[Option[User]] =
          (userActor ? GetUser(guid)).mapTo[Option[User]]
        rejectEmptyResponse {
          complete(maybeUser)
        }
      }
    } ~
      path("user") {
        get {
          parameters('skip.as[Int] ? 0, 'limit.as[Int] ? 20) { (skip, limit) =>
            val users = (userActor ? GetUsers(skip, limit)).mapTo[Future[Users]].flatten
            complete(users)
          }
        }
      } ~
      path("user" / Segment) { segment =>
        reject(ValidationRejection(s"$segment is not a valid guid."))
      }
}
