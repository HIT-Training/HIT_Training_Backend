package net.franksreich.user

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import net.franksreich.database.model._
import net.franksreich.shared.JsonSupport
import spray.json._

trait UserJsonSupport extends SprayJsonSupport with JsonSupport {
  import DefaultJsonProtocol._

  implicit object SexJsonFormat extends RootJsonFormat[Sex] {
    def write(sex: Sex) = JsString(sex.name)
    def read(value: JsValue) = value match {
      case JsString("Male") => Male
      case JsString("Female") => Female
      case x => deserializationError(s"$x is not a sex")
    }
  }

  implicit object RoleJsonFormat extends RootJsonFormat[Role] {
    def write(role: Role) = JsString(role.name)
    def read(value: JsValue) = value match {
      case JsString("Athlete") => Athlete
      case JsString("Trainer") => Trainer
      case x => deserializationError(s"$x is not a role")
    }
  }

  implicit val userJsonFormat = jsonFormat5(User)
  implicit val usersJsonFormat = jsonFormat1(Users)
}
