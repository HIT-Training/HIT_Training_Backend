package net.franksreich.user

import java.util.UUID

import akka.actor.{ Actor, ActorLogging }
import net.franksreich.database.Database
import net.franksreich.database.model.User

object UserActor {
  final case class GetUsers(skip: Int, limit: Int)
  final case class GetUser(name: UUID)
}

class UserActor(val database: Database) extends Actor with ActorLogging {
  import UserActor._

  var users = Set.empty[User]

  def receive: Receive = {
    case GetUsers(skip, limit) =>
      sender() ! database.userDataAccess.users(skip, limit)
    case GetUser(name) =>
      sender() ! users.find(_.name == name)
  }
}
