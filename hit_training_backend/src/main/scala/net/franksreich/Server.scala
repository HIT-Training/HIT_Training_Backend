package net.franksreich

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import akka.actor.{ ActorSystem, Props }
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import net.franksreich.database.DatabaseImplementation
import net.franksreich.exercise.{ ExerciseActor, ExerciseRoutes }
import net.franksreich.user.{ UserActor, UserRoutes }
import akka.util.Timeout
import net.franksreich.equipment.{ EquipmentActor, EquipmentRoutes }

import scala.concurrent.duration._

object Server extends App {
  implicit val system: ActorSystem = ActorSystem("HIT_Training_Backend")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val log = Logging.getLogger(system, this)
  val database = new DatabaseImplementation("mongodb://localhost", "HIT_Training", log)

  val userActor = system.actorOf(Props(new UserActor(database)), "userActor")
  val exerciseActor = system.actorOf(Props(new ExerciseActor(database)), "exerciseActor")
  val equipmentActor = system.actorOf(Props(new EquipmentActor(database)))

  val userRoutes = new UserRoutes(system, userActor, Timeout(5.seconds))
  val exerciseRoutes = new ExerciseRoutes(
    system, exerciseActor, Timeout(5.seconds), "http://localhost:8080")
  val equipmentRoutes = new EquipmentRoutes(
    system, equipmentActor, Timeout(5.seconds), "http://localhost:8080")

  lazy val routes = userRoutes.routes ~ exerciseRoutes.routes ~ equipmentRoutes.routes

  Http().bindAndHandle(routes, "localhost", 8080)

  Await.result(system.whenTerminated, Duration.Inf)
}
