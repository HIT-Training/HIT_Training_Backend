lazy val akkaHttpVersion = "10.1.4"
lazy val akkaVersion    = "2.5.15"
lazy val mongoDriverVersion = "2.1.0"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "net.franksreich",
      scalaVersion    := "2.12.6"
    )),
    name := "HIT_Training_Backend",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,
      "org.mongodb.scala" %% "mongo-scala-driver"   % mongoDriverVersion,

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test
    )
  )
